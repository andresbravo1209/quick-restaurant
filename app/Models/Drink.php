<?php

namespace App\Models;

use App\quickRestaurant\Relations\DrinkRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drink extends Model
{
    use HasFactory, SoftDeletes, DrinkRelations ;

    protected $fillable = [
        'restaurant_id',
        'name',
        'price',
        'of_the_day',
        'disabled',
    ];
}
