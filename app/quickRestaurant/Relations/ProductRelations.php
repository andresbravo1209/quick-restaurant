<?php

namespace App\quickRestaurant\Relations;

use App\Models\Dish;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait ProductRelations
{
    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function dishes(): belongsToMany
    {
        return $this->belongsToMany(Dish::class, 'dish_product');
    }
}
