<?php

namespace App\quickRestaurant\Relations;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait DrinkRelations
{
    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class);
    }
}
