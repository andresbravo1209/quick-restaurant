<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(): Response
    {
        $user = Auth::user();

        $restaurants = Restaurant::with('user')
            ->forUser($user)
            ->paginate(4);

        $onlineSells = 0;
        $cashSells = 0;
        $ordersCount = 0;
        /**@var Restaurant $restaurant*/
        foreach ($restaurants as $restaurant){
            $onlineTotal =  $restaurant->orders()->where('payment_method', 'online')->sum('total');
            $onlineSells = $onlineSells + $onlineTotal;

            $totalOrders = $restaurant->orders()->count();
            $ordersCount = $ordersCount + $totalOrders;

            $cashTotal =  $restaurant->orders()->where('payment_method', 'cash')->sum('total');
            $cashSells = $cashSells + $cashTotal;
        };

        return Inertia::render('Home', [
            'total_online' => $onlineSells,
            'total_cash' => $cashSells,
            'orders_count' => $ordersCount
        ]);
    }
}
