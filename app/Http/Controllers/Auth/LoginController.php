<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\Restaurant;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use App\Models\User;
use Inertia\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        auth()->logout();
        return response()->json([
            'data' => 'logout',
        ], 200);

        //return Inertia::render('Home');
    }

    public function showLoginForm(Restaurant $restaurant)
    {
        return Inertia::render('Auth/Login', [
            'restaurant' => $restaurant
        ]);
    }

    public function login(LoginRequest $request, Restaurant $restaurant)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();

            if (!$user->email_verified_at) {
                $errorMessage = 'Pendiente código de verificación';
                Auth::logout();
                return redirect()
                    ->route('login-restaurant', $restaurant)
                    ->with('errorMessage', $errorMessage);
            }

            if ($user->hasRole('client')){
                return redirect()
                    ->route('restaurant-menu', ['qrCode' => $restaurant->qr_code]);
            }

           return redirect()->intended($this->redirectPath());
        } else {

            $errorMessage = 'Por favor revisa tus credenciales';
            return redirect()
                ->route('login-restaurant', $restaurant)
                ->with('errorMessage', $errorMessage);
        }
    }

}
