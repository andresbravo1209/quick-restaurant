<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|max:10|min:10',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Tu correo electrónico es requerido',
            'email.email' => 'Tu correo electrónico debe ser uno valido!',
            'password.required' => 'Tu teléfono es requerido',
            'password.max' => 'Tu teléfono no tiene mas de 10 digitos',
            'password.min' => 'Tu teléfono debe tener 10 digitos',
        ];
    }
}
