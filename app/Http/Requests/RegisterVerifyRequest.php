<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'code' => 'required|string|min:4|max:4'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Tus Nombre es requerido para continuar!',
            'code.min' => 'Debe ser un código de valido de 4 dígitos',
            'code.max' => 'Debe ser un código de valido de 4 dígitos',
            'email.required' => 'Tu correo electrónico es requerido para continuar!',
            'email.email' => 'Tu correo electrónico debe ser uno valido!',
            'email.unique' => 'Ya tienes una cuenta existente!',
        ];
    }
}
