import {Card, CardGroup, Col, Container, Row} from "react-bootstrap";
import React from "react";
import DishCard from "../dishes/DishCard";

const CategoryCard = ({category,handleAdd}) => {

    const dishes = category.dishes;

    return (
        <Card style={{marginTop: 7}} className="text-center">
            <Card.Header >
                <b>{category.name}</b>
            </Card.Header>
            <Card.Body>
                <Container>
                    <CardGroup as='div'>
                    {dishes.length > 0 ? dishes.map(dish => (
                        <DishCard
                            handleAdd={handleAdd}
                            key={dish.id}
                            dish={dish}
                        />))
                        :
                        <p>Aun no hay platos en esta categoría.</p>}
                    </CardGroup>
                </Container>
            </Card.Body>
        </Card>
    );

};

export default CategoryCard;
