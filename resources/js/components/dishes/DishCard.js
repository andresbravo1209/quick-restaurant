import {Button, Card, Col, Container, Row} from "react-bootstrap";
import React from "react";
import {FaCartPlus} from "react-icons/fa";

const DishCard = ({ handleAdd, dish }) => {

    return (
        <Card style={{marginLeft: 2}} className="text-center">
            <Card.Header>
                <b>{dish.name}</b>
            </Card.Header>
            <Card.Img variant="top" src={dish.image? `/storage/${dish.image}` : '/storage/dishes/kjv4muosfaLQ3voYs56rJ5BZv1hW6C91Lahfe1EF.jpeg'} />
            <Card.Body>
                <Container>
                    <Row>
                        <Col>
                            {dish.products.map(p => p.name).join(', ')}
                        </Col>
                    </Row>
                </Container>
            </Card.Body>
            <Card.Footer className='text-center'>
                <b>{dish.price} COP</b>
                <Button
                    size='lg'
                    variant="secondary"
                    onClick={e => {handleAdd(e, dish, true)}}
                    block
                >
                    Agregar <FaCartPlus/>
                </Button>
            </Card.Footer>
        </Card>
    );
};


export default DishCard;
