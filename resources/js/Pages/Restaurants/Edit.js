import React, {useRef, useState} from "react";
import Title from "../../components/Title";
import {makeStyles} from "@material-ui/core/styles";
import Dashboard from "../../components/admin/Dashboard";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from 'react-select'
import SaveIcon from '@material-ui/icons/Save';
import { HalfCircleSpinner } from 'react-epic-spinners';
import {Inertia} from "@inertiajs/inertia";
import QRCode from 'qrcode.react';
import {useForm, usePage} from "@inertiajs/inertia-react";
import {Col, Container, Row} from "react-bootstrap";


const Restaurants = (props) => {

    const { restaurant, users, errors } = props;
    const { app_url } = usePage().props;

    const title = `Restaurante: ${restaurant.name}`;

    const imageRef = useRef(null);
    const [values, setValues] = useState({
        name: restaurant.name,
        address: restaurant.address,
        phone: restaurant.phone,
        user_id: restaurant.user_id,
        qr_code: restaurant.qr_code,
    });

    const { data, setData, processing } = useForm({
        name: restaurant.name,
        address: restaurant.address,
        phone: restaurant.phone,
        user_id: restaurant.user_id,
        qr_code: restaurant.qr_code,
    })

    const classes = useStyles();

    const handleOwnerId = (e) => {
        const key = e.value;
        setValues(values => ({
            ...values,
            user_id: key,
        }))
    }

    const handleSubmit = event => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('_method', 'put');
        formData.append('name', data.name);
        formData.append('address', data.address);
        formData.append('phone', data.phone);
        formData.append('user_id', data.user_id);
        formData.append('image_url', imageRef.current.files[0]);
        Inertia.post(`/restaurants/${restaurant.id}`, formData);
    }

    const tableRestaurants = (
        <>
            <Title>{title}</Title>
            <CssBaseline />
            <Container fluid>
                <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.name}
                            helperText={errors.name ?? false}
                            id="name"
                            label="Nombre"
                            defaultValue={restaurant.name}
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Nombre del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <label><b>Propietario</b></label>
                        <Select
                            className='mt-6'
                            id="user_id"
                            onChange={e => setData('user_id', e.value)}
                            options={users}
                            placeholder="Propietario"
                            defaultValue={users.filter(u => u.value === restaurant.user_id)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.address}
                            helperText={errors.address ?? false}
                            id="address"
                            label="Dirección"
                            defaultValue={restaurant.address}
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Dirección del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.phone}
                            helperText={errors.phone ?? false}
                            id="phone"
                            label="Teléfono"
                            defaultValue={restaurant.phone}
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Teléfono del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.qr_code}
                            helperText={errors.qr_code ?? false}
                            id="qr_code"
                            label="Codigo QR:"
                            defaultValue={restaurant.qr_code}
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="QR del Restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6} className='text-center'>
                        <QRCode
                            id="qrCodeEl"
                            size={150}
                            value={`${app_url}/restaurant/${data.qr_code}/menu`}
                        />
                    </Col>
                </Row>
               <Row>
                    <Col xs={12} md={6}>
                        <input
                            id='image_url'
                            ref={imageRef}
                            type="file"
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <Button
                            onClick={handleSubmit}
                            disabled={processing}
                            variant="contained"
                            color="primary"
                            size="small"
                            className={classes.button}
                            startIcon={<SaveIcon />}
                        >
                            { processing && <HalfCircleSpinner size={20} color={'#fff'} className="mr-2"/>}
                            Editar Restaurante
                        </Button>
                    </Col>
               </Row>
            </Container>
        </>
    );

    return (
        <Dashboard
            tableInformation={tableRestaurants}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    button: {
        margin: theme.spacing(1),
    },
}));

export default Restaurants;
