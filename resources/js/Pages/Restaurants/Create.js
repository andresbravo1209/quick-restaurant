import React from "react";
import Title from "../../components/Title";
import {makeStyles} from "@material-ui/core/styles";
import Dashboard from "../../components/admin/Dashboard";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from 'react-select'
import SaveIcon from '@material-ui/icons/Save';
import { HalfCircleSpinner } from 'react-epic-spinners';
import {Inertia} from "@inertiajs/inertia";
import {Col, Container, Row} from "react-bootstrap";
import {useForm} from "@inertiajs/inertia-react";

const RestaurantsCreate = (props) => {

    const { users, errors } = props;

    const title = `Crear Restaurante`;

    const { data, setData, processing } = useForm({
        name: '',
        address: '',
        phone: '',
        user_id: '',
    });

    const classes = useStyles();

    const handleSubmit = event => {
        event.preventDefault();
        Inertia.post(`/restaurants`, {
            name: data.name,
            address: data.address,
            phone: data.phone,
            user_id: data.user_id,
        });
    }

    const tableRestaurants = (
        <>
            <Title>{title}</Title>
            <CssBaseline />
            <Container fluid>
                <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            id="name"
                            error={!!errors.name}
                            helperText={errors.name ?? false}
                            label="Nombre"
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Nombre del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <Select
                            className='mt-6'
                            id="user_id"
                            onChange={e => setData('user_id', e.value)}
                            options={users}
                            placeholder="Propietario"
                        />
                        {errors.user_id && <p className='text-danger '>{errors.user_id}</p>}
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.address}
                            helperText={errors.address ?? false}
                            id="address"
                            label="Dirección"
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Dirección del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.phone}
                            helperText={errors.phone ?? false}
                            id="phone"
                            label="Teléfono"
                            onChange={e => setData(e.target.id, e.target.value)}
                            style={{ margin: 8 }}
                            placeholder="Teléfono del restaurante"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <Button
                            onClick={handleSubmit}
                            disabled={processing}
                            variant="contained"
                            color="primary"
                            size="large"
                            className={classes.button}
                            startIcon={<SaveIcon />}
                        >
                            { processing && <HalfCircleSpinner size={20} color={'#fff'} className="mr-2"/>}
                            Crear Restaurante
                        </Button>
                    </Col>
                </Row>
            </Container>
        </>
    );

    return (
        <Dashboard
            tableInformation={tableRestaurants}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    button: {
        margin: theme.spacing(1),
    },
}));

export default RestaurantsCreate;
