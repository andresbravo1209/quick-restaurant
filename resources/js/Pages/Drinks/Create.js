import React from "react";
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import {FormControlLabel, Switch} from "@material-ui/core";
import {Col, Container, Row} from "react-bootstrap";
import {useForm} from "@inertiajs/inertia-react";
import ButtonSubmit from "../../components/ButtonSubmit";

const DrinksCreate = (props) => {

    const { restaurant, errors } = props;

    const title = `Crear Bebida`;

    const { data, setData, post, processing } = useForm({
        name: '',
        price: '',
        of_the_day: false,
        disabled: false,
    });

    const handleChange = (e) => {
        setData(e.target.id, e.target.value)
    };

    const handleChangeSwitch = (e) => {
        setData(event.target.name,event.target.checked)
    };

    const handleSubmit = event => {
        event.preventDefault();
        post(`/restaurants/${restaurant.id}/drinks/create`, {
            name: data.name,
            price: data.price,
            of_the_day: data.of_the_day,
            disabled: data.disabled,
        });
    };

    const createDrink = (
        <>
            <Title>{title}</Title>
            <CssBaseline />
            <form onSubmit={handleSubmit}>
                <Container fluid>
                    <Row>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.name}
                                helperText={errors.name ?? false}
                                id="name"
                                label="Nombre"
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Nombre de la bebida"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.price}
                                helperText={errors.price ?? false}
                                id="price"
                                label="Precio"
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Precio de la bebida"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.of_the_day}
                                        onChange={handleChangeSwitch}
                                        name="of_the_day"
                                        id='of_the_day'
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                        color='primary'
                                    />
                                }
                                label="¿Bebida del día?"
                            />
                            {errors.of_the_day !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.of_the_day}</p>}
                        </Col>
                        <Col xs={12} md={6}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.disabled}
                                        onChange={handleChangeSwitch}
                                        name="disabled"
                                        id='disabled'
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                        color='primary'
                                    />
                                }
                                label="¿Deshabilitar?"
                            />
                            {errors.disabled !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.disabled}</p>}
                        </Col>
                    </Row>
                    <Col xs={12} md={6}>
                        <ButtonSubmit processing={processing} buttonText='Crear Bebida'/>
                    </Col>
                </Container>
            </form>
        </>
    );

    return (
        <Dashboard
            tableInformation={createDrink}
        />
    );
};

export default DrinksCreate;
