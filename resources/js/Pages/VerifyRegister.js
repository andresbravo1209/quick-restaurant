import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {PhonelinkRing} from "@material-ui/icons";
import {Alert} from "@material-ui/lab";
import {useForm, usePage} from "@inertiajs/inertia-react";
import SaveIcon from "@material-ui/icons/Save";
import {HalfCircleSpinner} from "react-epic-spinners";

const VerifyRegister = (props) => {

    const {errors, user} = props;
    const { session } = usePage().props

    const classes = useStyles();

    const { data, setData, processing, post } = useForm({
        code: "",
        email: user.email,
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post('/registers/verify', data);
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <PhonelinkRing />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Quick Restaurant
                </Typography>
                <h5>
                    Verificación de usuario
                </h5>
                {session.successMessage &&
                <Alert severity="success">
                    {session.successMessage}
                </Alert>}
                {session.errorMessage &&
                <Alert severity="error">
                    {session.errorMessage}
                </Alert>}
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                error={!!errors.code}
                                helperText={errors.code ?? false}
                                onChange={e => setData(e.target.id, e.target.value)}
                                variant="outlined"
                                required
                                fullWidth
                                id="code"
                                label="Codigo"
                                name="last_name"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={!!errors.email}
                                helperText={errors.email ?? false}
                                value={user.email}
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Correo electrónico"
                                name="email"
                                disabled
                            />
                        </Grid>
                    </Grid>
                    <Button
                        className='mt-2'
                        type='submit'
                        fullWidth
                        disabled={processing}
                        variant="contained"
                        color="primary"
                        startIcon={<SaveIcon />}
                    >
                        { processing && <HalfCircleSpinner size={20} color={'#fff'} className="mr-2"/>}
                        Continuar
                    </Button>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
};

const Copyright = () => {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Quick Restaurant
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    errorField: {
        marginTop: theme.spacing(2),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default VerifyRegister;
