import React from "react";
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import TextField from "@material-ui/core/TextField";
import Select from 'react-select'
import {Col, Container, Row} from "react-bootstrap";
import {useForm} from "@inertiajs/inertia-react";
import ButtonSubmit from "../../components/ButtonSubmit";

const UsersEdit = (props) => {

    const { user, roles, restaurants, errors } = props;

    const title = `Usuario: ${user.name}`;

    const userRoleId = user.roles.length > 0 ? user.roles[0].id : '';
    const userRestaurants = user.restaurants.map(r => r.id);

    const userRestaurantsFilter = restaurants.filter(({value}) => userRestaurants.includes(value));
    const userRestaurantsIds = userRestaurantsFilter.map(r => r.value);

    const { data, setData, patch, processing } = useForm({
        first_name: user.first_name,
        last_name: user.last_name,
        name: `${user.first_name} ${user.last_name}`,
        phone: user.phone,
        email: user.email,
        role_id: userRoleId,
        restaurants_id : userRestaurantsIds
    })

    const handleChange = (e) => {
        setData(e.target.id, e.target.value)
    };

    const handleRoleId = (e) => {
        setData('role_id', e.label)
    };

    const handleRestaurants = (e) => {
        const keys = e.map(r => r.value);
        setData('restaurants_id', keys)
    };

    const handleSubmit = event => {
        event.preventDefault();

        patch(`/users/${user.id}`, {
            first_name: data.first_name,
            last_name: data.last_name,
            name: `${data.first_name} ${data.last_name}`,
            phone: data.phone,
            email: data.email,
            role_id: data.role_id,
            restaurants_id: data.restaurants_id,
        }).then(r => {
            console.log('amiwis')
        });
    };

    const tableUsers = (
        <>
            <Title>{title}</Title>
            <form onSubmit={handleSubmit}>
                <Container fluid>
                    <Row>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.first_name}
                            helperText={errors.first_name ?? false}
                            id="first_name"
                            label="Nombres"
                            defaultValue={user.first_name}
                            onChange={handleChange}
                            style={{ margin: 8 }}
                            placeholder="Nombres"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                    <Col xs={12} md={6}>
                        <TextField
                            error={!!errors.last_name}
                            helperText={errors.last_name ?? false}
                            id="last_name"
                            label="Apellidos"
                            defaultValue={user.last_name}
                            onChange={handleChange}
                            style={{ margin: 8 }}
                            placeholder="Apellidos"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Col>
                </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Select
                                id="restaurants_id"
                                onChange={handleRestaurants}
                                options={restaurants}
                                placeholder="Restaurantes"
                                defaultValue={userRestaurantsFilter}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                isMulti
                            />
                            {errors.restaurants_id !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.restaurants_id}</p>}
                        </Col>
                        <Col xs={12} md={6}>
                            <Select
                                className='mt-6'
                                id="role_id"
                                onChange={handleRoleId}
                                options={roles}
                                placeholder="Rol"
                                defaultValue={roles.filter(r => r.value === userRoleId)}
                            />
                            {errors.role_id !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.role_id}</p>}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.email}
                                helperText={errors.email ?? false}
                                id="email"
                                label="Correo electrónico"
                                defaultValue={user.email}
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Email"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.phone}
                                helperText={errors.phone ?? false}
                                id="phone"
                                label="Teléfono"
                                defaultValue={user.phone}
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Teléfono del restaurante"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <ButtonSubmit
                               processing={processing}
                               buttonText='Editar usuario'
                           />
                        </Col>
                    </Row>
                </Container>
           </form>
        </>
    );

    return (
        <Dashboard
            tableInformation={tableUsers}
        />
    );
};

export default UsersEdit;
