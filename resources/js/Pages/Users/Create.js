import React from "react";
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import TextField from "@material-ui/core/TextField";
import Select from "react-select";
import {useForm} from "@inertiajs/inertia-react";
import ButtonSubmit from "../../components/ButtonSubmit";
import {Col, Container, Row} from "react-bootstrap";

const UsersCreate = (props) => {

    const title = `Crear Usuario`;
    const { restaurants, roles, errors } = props;

    const { data, setData, post, processing } = useForm({
        first_name: '',
        last_name: '',
        phone: '',
        email: '',
        role_id: 0,
        restaurants_id : []
    })

    const handleChange = (e) => {
        setData(e.target.id, e.target.value)
    };

    const handleRoleId = (e) => {
        setData('role_id', e.label)
    };

    const handleRestaurants = (e) => {
        const keys = e.map(r => r.value);
        setData('restaurants_id', keys)
    };

    const handleSubmit = event => {
        event.preventDefault();
        post(`/users`,
            {
                first_name: data.first_name,
                last_name: data.last_name,
                phone: data.phone,
                email: data.email,
                role_id: data.role_id,
                restaurants_id: data.restaurants_id
            });
    }

    const tableRestaurants = (
        <>
            <Title>{title}</Title>
            <form onSubmit={handleSubmit}>
                <Container fluid>
                    <Row>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.first_name}
                                helperText={errors.first_name ?? false}
                                id="first_name"
                                label="Nombres"
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Nombres"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                autoComplete='off'
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.last_name}
                                helperText={errors.last_name ?? false}
                                id='last_name'
                                label='Apellidos'
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder='Apellidos'
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                autoComplete='off'
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Select
                                id='restaurants_id'
                                onChange={handleRestaurants}
                                options={restaurants}
                                placeholder="Restaurantes"
                                className="basic-multi-select"
                                classNamePrefix="select"
                                isMulti
                            />
                            {errors.restaurants_id !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.restaurants_id}</p>}
                        </Col>
                        <Col xs={12} md={6}>
                            <Select
                                className='mt-6'
                                id="role_id"
                                onChange={handleRoleId}
                                options={roles}
                                placeholder="Rol"
                                defaultValue={roles.filter(r => r.label === 'client')}
                            />
                            {errors.role_id !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.role_id}</p>}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.email}
                                helperText={errors.email ?? false}
                                id='email'
                                label='Email'
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder='Correo electrónico'
                                fullWidth
                                margin="normal"
                                InputLabelProps={{shrink: true,}}
                                autoComplete='off'
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.phone}
                                helperText={errors.phone ?? false}
                                id='phone'
                                label='Teléfono'
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder='Teléfono'
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                autoComplete='off'
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={12}>
                            <ButtonSubmit
                                processing={processing}
                                buttonText={'Crear usuario'}
                            />
                        </Col>
                    </Row>
                </Container>
            </form>
        </>
    );

    return (
        <Dashboard
            tableInformation={tableRestaurants}
        />
    );
}

export default UsersCreate;
