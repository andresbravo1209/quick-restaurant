import React from "react";
import { Inertia } from '@inertiajs/inertia'
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import {useForm} from "@inertiajs/inertia-react";
import Pagination from "../../components/Pagination";
import {
    InputAdornment,
    TextField,
} from "@material-ui/core";
import {Search} from "@material-ui/icons";
import {Col, Container, Dropdown, Row, Table} from "react-bootstrap";
import {Button as BButton} from "react-bootstrap";

const Users = (props) => {

    const { users } = props;
    const title = 'Usuarios'

    const { data, setData } = useForm({
        search: '',
    })

    const handleSearch = (e) => {
        e.preventDefault();
        Inertia.visit('/users', {
            method: 'get',
            data: {
                search: data.search,
            },
        })
    };

    const cardsUsers = (
        <>
            <Row className='mb-2'>
                <Col>
                    <Title>{title}</Title>
                </Col>
                <Col className='text-right'>
                    <a href="/users/create" className="btn btn-primary mr-5">Crear Usuario</a>
                </Col>
                <Col className='text-center'>
                    <TextField
                        onChange={e => setData(e.target.id, e.target.value)}
                        id="search"
                        label="Búsqueda"
                        type='search'
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Search />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <BButton type='button' variant="light" onClick={handleSearch}>Buscar</BButton>
                </Col>
            </Row>
            <Container fluid>
                <Row>
                    <Table striped bordered hover size="sm">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Información</th>
                            <th>Detalles</th>
                        </tr>
                        </thead>
                        <tbody>
                        {users.data.length > 0 && users.data.map(user => (
                            <tr key={user.id}>
                                <td>{user.name}</td>
                                <td>
                                    Teléfono: <b>{user.phone}</b><br/>
                                    Email: <b>{user.email}</b><br/>
                                    Rol: <b>{user.roles[0].name}</b>
                                </td>
                                <td className='text-center'>
                                    <Dropdown>
                                        <Dropdown.Toggle variant="success" id="actions-restaurant">Acciones</Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item href={`/users/${user.id}/edit`}>Editar</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Row>
                <Row className='mt-3'>
                    <Col xs={12}>
                        <Pagination links={users.links}/>
                    </Col>
                </Row>
            </Container>
        </>
    );

    return (
        <Dashboard
            tableInformation={cardsUsers}
        />
    );
};

export default Users;
