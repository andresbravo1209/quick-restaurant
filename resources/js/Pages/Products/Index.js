import React from "react";
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import Pagination from "../../components/Pagination";
import {Col, Container, Dropdown, Row, Table} from "react-bootstrap";
import {Inertia} from "@inertiajs/inertia";

const Products = (props) => {

    const { products, restaurant } = props;
    const title = 'Productos'

    const cardsProducts = (
        <>
            <div className='row mb-2'>
                <div className='col'>
                    <Title>{title}</Title>
                </div>
                <div className='col text-right'>
                    <a href={`/restaurants/${restaurant.id}/products/create`} className="btn btn-primary mr-5">Crear producto</a>
                </div>
            </div>
            <Container fluid>
                <Row>
                    <Table striped bordered hover size="sm">
                        <thead>
                        <tr>
                            <th>Nombre</th><th>Información</th><th>Detalles</th>
                        </tr>
                        </thead>
                        <tbody>
                        {products.data.length > 0 && products.data.map(product => (
                            <tr key={product.id}>
                                <td>{product.name}</td>
                                <td>Precio: <b>{product.price}</b></td>
                                <td className='text-center'>
                                    <Dropdown>
                                        <Dropdown.Toggle variant="success" id="actions-restaurant">
                                            Acciones
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item href={`/restaurants/${restaurant.id}/products/${product.id}/edit`}>Editar</Dropdown.Item>
                                            <Dropdown.Item
                                                onClick={() =>{
                                                    swal({
                                                        title: "Borrar este producto?",
                                                        text: `Estas a punto de borrar el producto ${product.name}!`,
                                                        icon: "warning",
                                                        buttons: true,
                                                        dangerMode: true,
                                                    })
                                                        .then((willDelete) => {
                                                            if (willDelete) {
                                                                Inertia.delete(`/restaurants/${restaurant.id}/products/${product.id}/delete`)
                                                                swal("Producto Borrado!", {
                                                                    icon: "success",
                                                                });
                                                            }
                                                        });
                                                }}>
                                                Borrar
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Row>
                <Row className='mt-3'>
                    <Col xs={12}>
                        <Pagination links={products.links}/>
                    </Col>
                </Row>
            </Container>

        </>
    );

    return (
        <Dashboard
            tableInformation={cardsProducts}
        />
    );
}

export default Products;
