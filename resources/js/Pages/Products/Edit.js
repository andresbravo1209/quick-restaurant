import React from "react";
import Title from "../../components/Title";
import Dashboard from "../../components/admin/Dashboard";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import {FormControlLabel, Switch} from "@material-ui/core";
import {Col, Container, Row} from "react-bootstrap";
import {useForm} from "@inertiajs/inertia-react";
import ButtonSubmit from "../../components/ButtonSubmit";

const ProductsEdit = (props) => {

    const { restaurant, product, errors } = props;

    const title = `Editar Producto`;

    const { data, setData, patch, processing } = useForm({
        name: product.name,
        price: product.price?? '0',
        has_additional_price: product.has_additional_price,
        restaurant_id: restaurant.id,
    });

    const handleChange = (e) => {
        setData(e.target.id, e.target.value)
    };

    const handleChangeSwitch = (e) => {
        setData(event.target.name,event.target.checked)
    };

    const handleSubmit = event => {
        event.preventDefault();
        patch(`/restaurants/${product.restaurant_id}/products/${product.id}`, {
            name: data.name,
            price: data.price,
            has_additional_price: data.has_additional_price,
            restaurant_id: data.restaurant_id,
        });
    };

    const createProduct = (
        <>
            <Title>{title}</Title>
            <CssBaseline />
            <form onSubmit={handleSubmit}>
                <Container fluid>
                    <Row>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.name}
                                helperText={errors.name ?? false}
                                id="name"
                                label="Nombre"
                                defaultValue={product.name}
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Nombre del producto"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                        <Col xs={12} md={6}>
                            <TextField
                                error={!!errors.price}
                                helperText={errors.price ?? false}
                                defaultValue={product.price?? '0'}
                                id="price"
                                label="Precio"
                                onChange={handleChange}
                                style={{ margin: 8 }}
                                placeholder="Precio del producto"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={data.has_additional_price}
                                        onChange={handleChangeSwitch}
                                        name="has_additional_price"
                                        id='has_additional_price'
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                        color='primary'
                                    />
                                }
                                label="¿Precio adicional?"
                            />
                            {errors.has_additional_price !== null &&
                            <p style={{fontSize: '0.75 rem'}} className='text-danger'>{errors.has_additional_price}</p>}
                        </Col>
                    </Row>
                    <Col xs={12} md={6}>
                        <ButtonSubmit processing={processing} buttonText='Crear Producto'/>
                    </Col>
                </Container>
            </form>
        </>
    );

    return (
        <Dashboard
            tableInformation={createProduct}
        />
    );
};

export default ProductsEdit;
