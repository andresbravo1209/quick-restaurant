import React, {useState} from 'react';
import Alert from "react-bootstrap/Alert";
import {
    Accordion,
    Button,
    Card,
    Col,
    Container,
    ListGroup,
    Modal,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import CategoryCard from "../components/categories/CategoryCard";
import useLocalStorage from "../hooks/useLocalStorage";
import {Inertia} from "@inertiajs/inertia";
import {useForm, usePage} from "@inertiajs/inertia-react";
import ModalOrder from "../components/orders/ModalOrder";
import ModalDrinks from "../components/orders/ModalDrinks";
import moment from "moment/moment";

moment.locale('es');
const Menu = ({ restaurant, drinks, errors }) => {

    const { authUser } = usePage().props;

    const orderRestaurant = `order-restaurant-${restaurant.id}`;
    const [order, setOrder] = useLocalStorage(orderRestaurant, []);
    const [modalShow, setModalShow] = useState(false);
    const [modalDrinksShow, setModalDrinksShow] = useState(false);
    const [errorShow, setErrorShow] = useState(true);
    const [drink, setDrink] = useState(null);
    const [dishUser, setDishUser] = useState(null);

    const [showOrders, setShowOrders] = useState(false);
    const handleClose = () => setShowOrders(false);
    const handleShow = () => setShowOrders(true);

    const [orders, setOrders] = useState([]);

    const { data, setData, processing } = useForm({
        details: order,
        restaurant_id: restaurant.id,
        comment: '',
    })

    const emptyCar = order.length >= 1;
    const categories = restaurant.categories;

    const handleClick = (eventKey, id) => {
        switch (eventKey) {
            case 'orders':
                axios.get(`/api/orders/${id}`,{})
                    .then(res => {
                        if(res.data.status){
                            setOrders(res.data.data.orders);
                            handleShow();
                        }
                    });
                break;
            case 'logout':
                axios.post('/logout').then(response => {
                    //  Handling the response (Show a success message etc)
                    window.location = "/"
                }).catch(error => {
                    console.log(error, 'Desde error');
                    //  Handling the response (Show an error notification etc)
                    if (error.response) { // You can also check the status i.e 422
                        console.log(error.response, 'desde response')
                    }
                })
                // expected output: "Mangoes and papayas are $2.79 a pound."
                break;
            default:
                console.log(`Sorry, we are out of ${eventKey}.`);
        }
    };

    const removeDish = (e,dish, index) => {
        e.preventDefault();
        /*const dishExists = order.find(o => o.dish_id === dish.dish_id);
        console.log(dishExists, order, dish);
        if (dishExists && dishExists.quantity > 1){
            const newOrder = [... order];
            newOrder[index] = {...newOrder[index], quantity: newOrder[index].quantity-1}
            //console.log('restando', newOrder[index])
            setOrder(newOrder);
        }else {

        }*/
        const newOrder = [... order];
        newOrder.splice(index, 1);
        //console.log('quitando', index);
        setData('details', [... newOrder])
        setOrder(order => [... newOrder]);
    };

    const handleAddDish = (e,dish, drink) => {
        e.preventDefault();
        /*const dishExists = order.findIndex(o => o.dish_id === dish.id);
        if (dishExists !== -1){
            const oldOrder = [... order];
            oldOrder[dishExists] = {...oldOrder[dishExists], quantity: oldOrder[dishExists].quantity+1}
            setOrder(oldOrder);
            swal({
                title: "Plato agregado!",
                text: oldOrder[dishExists].dish_name,
                icon: "success",
                button: "Listo!",
            });
        }else {

        }*/
        if (!dish || !drink) return;

        const newDish = {
            dish_id: dish.id,
            dish_name: dish.name,
            quantity: 1,
            price: dish.price,
            drink_id: drink.id,
            drink_price: drink.price,
            drink_name: drink.name,
        };
        setOrder(order => [... order, newDish]);
        setData('details', [...order, newDish])
        swal({
            title: "Plato agregado!",
            text: newDish.dish_name,
            icon: "success",
            button: "Listo!",
        });
    };

    const handleConfirmOrder = (e) => {
        e.preventDefault();
        Inertia.post(`/orders/create`, data);
        setModalShow(false);
    };

    const handleAdd = (e, dish, showDrinks) => {
        if (!dish) return;
        if (!authUser){
            Inertia.get(`/register/${restaurant.id}`);
        }else {
            setModalDrinksShow(showDrinks);
            setDishUser(dish)
        }
    };

    return (<>
        <Navbar
            collapseOnSelect
            expand="lg"
            bg="primary"
            variant="dark"
            onSelect={(selectedKey) => handleClick(selectedKey, authUser.id)}
        >
            <Navbar.Brand href="#">QR - {restaurant.name}</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            {authUser && <>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="#" eventKey='orders'><b>Ordenes</b></Nav.Link>
                        <Nav.Link href="#" eventKey='logout'><b>Cerrar Sesión</b></Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Button
                    variant={emptyCar? 'danger' : 'light'}
                    onClick={() => setModalShow(true)}
                >
                    Mi Orden {emptyCar? order.length : ''}
                </Button>
            </>}
        </Navbar>
        <Container>
            {Object.entries(errors).length !== 0 &&
            <Alert show={errorShow} variant="warning">
                <Alert.Heading>Up's algo esta mal en tu orden?!</Alert.Heading>
                <p>
                   Intenta vaciar tu compra y reordenar!
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={() => setErrorShow(false)} variant="outline-success">
                        Vaciar!
                    </Button>
                </div>
            </Alert>}
            {categories.length > 0 ? categories.map(category => (
                <Row key={category.id}>
                    <Col sm={12} md={12}>
                        <CategoryCard
                            handleAdd={handleAdd}
                            category={category}
                        />
                    </Col>
                </Row>
            )) : <p>Sin categorías para mostrar.</p>}
            <Modal show={showOrders} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Mis Ordenes</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {orders.length > 0? orders.map(order => (
                        <Row key={order.id} className='mt-2'>
                            <Col>
                                <Accordion>
                                    <Card>
                                        <Card.Body>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Fecha:</b> {moment(order.created_at).format('dddd, MMMM Do YYYY')}</Card.Subtitle>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Hora:</b> {moment(order.created_at).format('h:mm:ss a')}</Card.Subtitle>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Restaurante:</b> {order.restaurant.name}</Card.Subtitle>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Id:</b> {order.id.slice(-4)}</Card.Subtitle>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Precio:</b> {order.total} COP</Card.Subtitle>
                                            <Card.Subtitle className="mb-2 text-muted"><b>Comentarios:</b> {order.comment}</Card.Subtitle>
                                        </Card.Body>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                            Ver Detalles
                                        </Accordion.Toggle>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Footer>
                                                <ListGroup as="ul">
                                                    <ListGroup.Item as="li" active>
                                                        Platos Y Bebidas
                                                    </ListGroup.Item>
                                                    {order.details.length > 0 && order.details.map((detail, index) => (
                                                        <ListGroup.Item key={index} as="li">{detail.dish_name} - {detail.drink_name}</ListGroup.Item>
                                                    ))}
                                                </ListGroup>
                                            </Card.Footer>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                            </Col>
                        </Row>
                    )) : <p>SIN ORDENES</p>}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
            <ModalOrder
                order={order}
                modalShow={modalShow}
                setModalShow={setModalShow}
                onRemove={removeDish}
                data={data}
                setData={setData}
                handleConfirm={handleConfirmOrder}
                processing={processing}
            />
            <ModalDrinks
                dish={dishUser}
                drink={drink}
                setDrink={setDrink}
                drinks={drinks}
                modalDrinksShow={modalDrinksShow}
                setModalDrinksShow={setModalDrinksShow}
                handleAdd={handleAddDish}
            />
        </Container>
        </>);
};

export default Menu;
