import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dashboard from "../components/admin/Dashboard";
import {Paper} from "@material-ui/core";
import {Col, Container, Row} from "react-bootstrap";

const HomePage = ({total_online, total_cash, orders_count}) => {
    const classes = useStyles();

    const tableInformation = (
        <div className={classes.root}>
                <Container fluid>
                    <Row>
                        <Col xs={6}>
                            <Paper className={classes.paper}>
                                <button type="button" className="btn btn-success">
                                    Ordenes Aceptadas: <span className="badge bg-secondary">{orders_count}</span>
                                </button>
                            </Paper>
                        </Col>
                        <Col xs={6}>
                            <Paper className={classes.paper}>
                                <button type="button" className="btn btn-info">
                                    Usuarios Activos: <span className="badge bg-light">4</span>
                                </button>
                            </Paper>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6}>
                            <Paper className={classes.paper}>
                                <button type="button" className="btn btn-success">
                                    Total Ventas Efectivo: <span className="badge bg-secondary">{total_cash} COP</span>
                                </button>
                            </Paper>
                        </Col>
                        <Col xs={6}>
                            <Paper className={classes.paper}>
                                <button type="button" className="btn btn-success">
                                    Total Ventas Online: <span className="badge bg-secondary">{total_online} COP</span>
                                </button>
                            </Paper>
                        </Col>
                    </Row>
                </Container>
            </div>);

    return (
        <Dashboard
            tableInformation={tableInformation}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default HomePage;
