import React from 'react';
import { Inertia } from '@inertiajs/inertia'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {VpnKey} from "@material-ui/icons";
import {InputAdornment, Snackbar} from "@material-ui/core";
import {Alert} from "@material-ui/lab";
import {useForm, usePage} from "@inertiajs/inertia-react";

const Login = (props) => {

    const {errors, restaurant} = props;
    const { session } = usePage().props

    const classes = useStyles();

    const { data, setData, processing } = useForm({
        email: "",
        password: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        Inertia.post(`/login/${restaurant.id}`, data);
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <VpnKey />
                </Avatar>
                <Typography component="h2" variant="h5">
                    Quick Restaurant
                </Typography>
                <Typography component="h1" variant="h5">
                    Ingresa tus datos.
                </Typography>
                {session.errorMessage &&
                <Alert severity="error">
                    {session.errorMessage}
                </Alert>}
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                error={!!errors.email}
                                helperText={errors.email ?? false}
                                onChange={e => setData(e.target.id, e.target.value)}
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Correo electrónico"
                                name="email"
                                autoComplete='off'
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={!!errors.password}
                                helperText={errors.password ?? false}
                                onChange={e => setData(e.target.id, e.target.value)}
                                variant="outlined"
                                required
                                fullWidth
                                id="password"
                                label="Teléfono"
                                name="password"
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">+57</InputAdornment>,
                                }}
                                autoComplete='off'
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={processing}
                    >
                        Continuar
                    </Button>
                    <Grid container justify='center'>
                        <Grid item>
                            <Button
                                href={`/register/${restaurant.id}`}
                                type="button"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                disabled={processing}
                            >
                                Registrate aquí!
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
};

const Copyright = () => {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Quick Restaurant
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    errorField: {
        marginTop: theme.spacing(2),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default Login;
