import React, {useState} from "react";
import Title from "../../components/Title";
import {makeStyles} from "@material-ui/core/styles";
import Dashboard from "../../components/admin/Dashboard";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    Collapse,
    Grid,
    IconButton, InputAdornment,
    TextField,
    Typography
} from "@material-ui/core";
import Pagination from "../../components/Pagination";
import { Inertia } from "@inertiajs/inertia";
import {Alert, AlertTitle} from "@material-ui/lab";
import {RefreshButton} from "./Checkout";
import {ExpandMore, NotInterested, Restaurant, Search, ThumbUp} from "@material-ui/icons";
import clsx from 'clsx';
import {useForm, usePage} from "@inertiajs/inertia-react";
import {Button as BButton, Col, Container, ListGroup, Row} from "react-bootstrap";
import moment from "moment/moment";

moment.locale('es');
const Orders = (props) => {

    const { orders, restaurant } = props;
    const { has_permission } = usePage().props;
    const title = 'Ordenes'
    const classes = useStyles();

    const { data, setData } = useForm({
        search: '',
    })
    const handleSearch = (e) => {
        e.preventDefault();
        Inertia.visit(`/restaurants/${restaurant.id}/orders`, {
            method: 'get',
            data: {
                search: data.search,
            },
        })
    };

    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const handleChangeState = (e, id, state) => {
        e.preventDefault();
        Inertia.post(`/orders/${id}/status/admin`, {
            status: state
        })
    };

    const OrderCards = (
        <>
            <Row className='mb-2'>
                <Col>
                    <Title>{title}</Title>
                </Col>
                <Col className='text-right'>
                    <RefreshButton title='Ordenes'/>
                </Col>
                <Col className='text-center'>
                    <TextField
                        onChange={e => setData(e.target.id, e.target.value)}
                        id="search"
                        label="Búsqueda"
                        type='search'
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Search />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <BButton type='button' variant="light" onClick={handleSearch}>Buscar</BButton>
                </Col>
            </Row>
            <Container>
                <Row >
                {orders.data.length > 0 ? orders.data.map(order => (
                    <Col key={order.id} md={6} sm={12}>
                            <Card className='mt-2'>
                                <CardContent>
                                    <Row>
                                        <Col className='text-right'>
                                            <b>Fecha del pedido: </b>{moment(order.created_at).format('dddd, MMMM Do YYYY')}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className='text-right'>
                                            <b>Hora del pedido: </b>{moment(order.created_at).format('h:mm:ss a')}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Orden: </b>{order.id.slice(-4)}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Estado: </b>
                                            {order.status === 'cooking' &&
                                            <span className="badge bg-warning text-dark">COCINANDO</span>}
                                            {order.status === 'pending' &&
                                            <span className="badge bg-warning text-dark">PENDIENTE</span>}
                                            {order.status === 'cash' &&
                                            <span className="badge bg-warning text-dark">PENDIENTE DE PAGO EN CAJA</span>}
                                            {order.status === 'paid' &&
                                            <span className="badge bg-primary">PAGADO</span>}
                                            {order.status === 'finished' &&
                                            <span className="badge bg-success">COMPLETADO</span>}
                                            {order.status === 'canceled_by_restaurant' &&
                                            <span className="badge bg-danger">CANCELADO POR RESTAURANTE</span>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Precio:</b> {order.total} COP
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Tipo de pago: </b>
                                            {order.payment_method === 'cash' &&
                                            <span className="badge bg-info text-dark">EFECTIVO</span>}
                                            {order.payment_method === 'online' &&
                                            <span className="badge bg-info text-dark">ONLINE</span>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Usuario: </b> {order.user.name}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <b>Comentarios: </b> {order.comment}
                                        </Col>
                                    </Row>
                                </CardContent>
                                <CardActions>
                                    <Row>
                                        <Col>
                                            {order.status === 'cash' || order.status === 'pending' || order.status === 'paid'?
                                            <Button
                                                onClick={(e) => handleChangeState(e,order.id, 'cooking')}
                                                size='small'
                                                color="primary"
                                                startIcon={<Restaurant/>}
                                            >
                                                Preparar
                                            </Button> : null}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            {order.status === 'cash' || order.status === 'pending' && has_permission &&
                                            <Button
                                                onClick={(e) => handleChangeState(e,order.id, 'canceled_by_restaurant')}
                                                size='small'
                                                color="secondary"
                                                startIcon={<NotInterested/>}
                                            >
                                                Cancelar
                                            </Button>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            {order.status === 'cooking' &&
                                            <Button
                                                onClick={(e) => handleChangeState(e,order.id, 'finished')}
                                                size='small'
                                                color="primary"
                                                startIcon={<ThumbUp/>}
                                            >
                                                Entregado
                                            </Button>}
                                        </Col>
                                    </Row>
                                    <IconButton
                                        className={clsx(classes.expand, {[classes.expandOpen]: expanded,})}
                                        onClick={handleExpandClick}
                                        aria-expanded={expanded}
                                        aria-label="show details"
                                    >
                                        <ExpandMore/>
                                    </IconButton>
                                </CardActions>
                                <Collapse in={expanded} timeout="auto" unmountOnExit>
                                    <CardContent>
                                        <Row>
                                            <Col>
                                                <ListGroup as="ul">
                                                    <ListGroup.Item as="li" active>
                                                        Platos Y Bebidas
                                                    </ListGroup.Item>
                                                    {order.details.length > 0 && order.details.map((detail, index) => (
                                                        <ListGroup.Item
                                                            key={index}
                                                            as="li"
                                                        >
                                                            {detail.dish_name} - {detail.drink_name}
                                                        </ListGroup.Item>
                                                    ))}
                                                </ListGroup>
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Collapse>
                            </Card>
                        </Col>

                    )) :
                    <Col  item xs={6} sm={12}>
                        <Alert severity="info">
                            <AlertTitle>Aun no hay ordenes creadas</AlertTitle>
                        </Alert>
                    </Col>}
                </Row>
                <Row className='mt-2'>
                    <Col xs={12}>
                        <Pagination links={orders.links}/>
                    </Col>
                </Row>
            </Container>

        </>
    );

    return (
        <Dashboard
            tableInformation={OrderCards}
        />
    );
};

const useStyles = makeStyles((theme) => ({
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
}));

export default Orders;
